package com.jalauniversity.sd3labweek7victorraphael.services;

import com.jalauniversity.sd3labweek7victorraphael.dtos.TaskRecordDto;
import com.jalauniversity.sd3labweek7victorraphael.models.TaskModel;
import com.jalauniversity.sd3labweek7victorraphael.repositories.TasksRepository;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.UUID;

@Service
public class TaskService {

    @Autowired
    private TasksRepository tasksRepository;

    @Autowired
    private HttpServletRequest request;

    public TaskModel createTask(TaskRecordDto taskRecordDto) {
        TaskModel taskModel = new TaskModel();
        taskModel.setDescription(taskRecordDto.description());
        taskModel.setTitle(taskRecordDto.title());
        taskModel.setPriority(taskRecordDto.priority());

        // Obter o userId da requisição
        UUID userId = (UUID) request.getAttribute("idUser");
        taskModel.setUserId(userId);

        return tasksRepository.save(taskModel);
    }

    public List<TaskModel> listTasks(UUID userId) {
        return tasksRepository.findByUserId(userId);
    }

}

