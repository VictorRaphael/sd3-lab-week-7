package com.jalauniversity.sd3labweek7victorraphael.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.jalauniversity.sd3labweek7victorraphael.dtos.UserRecordDto;
import com.jalauniversity.sd3labweek7victorraphael.models.UserModel;
import com.jalauniversity.sd3labweek7victorraphael.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public UserModel CreateUser(UserRecordDto userRecordDto){

         UserModel userModel = new UserModel();
         userModel.setUserName(userRecordDto.userName());
         userModel.setUserEmail(userRecordDto.userEmail());

         var passwordHashed= BCrypt.withDefaults().hashToString(12, userRecordDto.userPassword().toCharArray());
         userModel.setUserPassword(passwordHashed);

         return userRepository.save(userModel);
    }
}
