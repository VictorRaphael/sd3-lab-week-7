package com.jalauniversity.sd3labweek7victorraphael.security;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.jalauniversity.sd3labweek7victorraphael.repositories.UserRepository;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Base64;

@Component
public class FilterTaskAuth extends OncePerRequestFilter {

    @Autowired
    UserRepository userRepository;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String servletPath = request.getServletPath();

        if (servletPath.equals("/task/")) {

            String authorization = request.getHeader("Authorization");

            if (authorization != null && authorization.startsWith("Basic ")) {
                String authEncoded = authorization.substring("Basic ".length()).trim();
                byte[] authDecode = Base64.getDecoder().decode(authEncoded);
                String authString = new String(authDecode);

                String[] credentials = authString.split(":");
                String userName = credentials[0];
                String userPassword = credentials[1];

                var user = this.userRepository.findByUserName(userName);

                if (user != null && BCrypt.verifyer().verify(userPassword.toCharArray(), user.getUserPassword()).verified) {
                    request.setAttribute("idUser", user.getId());
                    filterChain.doFilter(request, response);
                } else {
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Usuário não autorizado");
                }
            } else {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Credenciais não fornecidas");
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }

}
