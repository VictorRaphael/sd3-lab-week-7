package com.jalauniversity.sd3labweek7victorraphael;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sd3LabWeek7VictorRaphaelApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sd3LabWeek7VictorRaphaelApplication.class, args);
    }

}
