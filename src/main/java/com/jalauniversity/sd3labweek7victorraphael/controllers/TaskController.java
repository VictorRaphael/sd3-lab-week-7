package com.jalauniversity.sd3labweek7victorraphael.controllers;

import com.jalauniversity.sd3labweek7victorraphael.dtos.TaskRecordDto;
import com.jalauniversity.sd3labweek7victorraphael.models.TaskModel;
import com.jalauniversity.sd3labweek7victorraphael.services.TaskService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    TaskService taskService;

    @PostMapping("/")
    public ResponseEntity<TaskModel> createTask(@Valid @RequestBody TaskRecordDto taskRecordDto) {
        TaskModel taskCreated = taskService.createTask(taskRecordDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(taskCreated);
    }

    @GetMapping("/")
    public ResponseEntity<List<TaskModel>> listTasks(HttpServletRequest request) {
        UUID userId = (UUID) request.getAttribute("idUser");
        List<TaskModel> taskList = taskService.listTasks(userId);
        return ResponseEntity.ok(taskList);
    }
}




