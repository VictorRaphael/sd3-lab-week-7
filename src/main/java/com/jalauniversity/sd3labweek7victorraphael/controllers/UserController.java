package com.jalauniversity.sd3labweek7victorraphael.controllers;

import com.jalauniversity.sd3labweek7victorraphael.dtos.UserRecordDto;
import com.jalauniversity.sd3labweek7victorraphael.models.UserModel;
import com.jalauniversity.sd3labweek7victorraphael.repositories.UserRepository;
import com.jalauniversity.sd3labweek7victorraphael.services.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @PostMapping("/")
    public ResponseEntity createUser(@Valid @RequestBody UserRecordDto userRecordDto) {
        UserModel existingUser = userRepository.findByUserName(userRecordDto.userName());

        if (existingUser != null) {
            return ResponseEntity.badRequest().body("O usuário já existe!");
        }

        var createdUser = userService.CreateUser(userRecordDto);

        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }
}

