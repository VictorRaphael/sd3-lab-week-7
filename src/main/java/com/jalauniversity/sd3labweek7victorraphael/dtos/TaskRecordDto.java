package com.jalauniversity.sd3labweek7victorraphael.dtos;

import jakarta.validation.constraints.NotNull;

public record TaskRecordDto(@NotNull String description, @NotNull String title, @NotNull String priority) {

}
