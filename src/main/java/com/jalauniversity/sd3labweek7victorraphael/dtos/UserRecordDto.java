package com.jalauniversity.sd3labweek7victorraphael.dtos;

import jakarta.validation.constraints.NotNull;

public record UserRecordDto(@NotNull String userName, @NotNull String userPassword, @NotNull String userEmail) {

}
