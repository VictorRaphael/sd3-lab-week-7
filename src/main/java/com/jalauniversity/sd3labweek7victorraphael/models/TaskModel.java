package com.jalauniversity.sd3labweek7victorraphael.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Entity(name = "tb_tasks")
@Data
@NoArgsConstructor
@AllArgsConstructor

public class TaskModel {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "taskId")
    private UUID id;

    @Column(name = "description")
    private String description;

    @Column(name = "title")
    private String title;

    @Column(name = "priority")
    private String priority;

    @Column(name = "userId")
    private UUID userId;
}
