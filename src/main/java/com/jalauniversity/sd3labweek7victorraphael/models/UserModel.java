package com.jalauniversity.sd3labweek7victorraphael.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;


@Entity(name = "tb_users")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserModel {

    @GeneratedValue(strategy = GenerationType.UUID)
    @Id
    @Column(name = "userId")
    private UUID id;


    @Column(name = "userName", length = 70, unique = true)
    private String userName;

    @Column(name = "userPassword", length = 70)
    private String userPassword;


    @Column(name = "userEmail")
    private String userEmail;
}
