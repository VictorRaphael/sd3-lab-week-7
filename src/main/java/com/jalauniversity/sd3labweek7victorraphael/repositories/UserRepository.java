package com.jalauniversity.sd3labweek7victorraphael.repositories;

import com.jalauniversity.sd3labweek7victorraphael.models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserModel, UUID> {

    UserModel findByUserName(String userName);
}
