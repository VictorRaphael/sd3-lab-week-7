package com.jalauniversity.sd3labweek7victorraphael.repositories;

import com.jalauniversity.sd3labweek7victorraphael.models.TaskModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TasksRepository extends JpaRepository<TaskModel, UUID> {
    List<TaskModel> findByUserId(UUID idUser);
}
